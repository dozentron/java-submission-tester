module JavaSubmissionTester
  class PagesController < ::ApplicationController
    layout "application"

    def show
      render action: params[:page]
    end
  end
end