package de.thm.mni;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/** Finds all test methods in the given class (args[0]).
 * Created by dominic-althaus on 19.05.16.
 * @param cli_args[0] the test class name
 */
public class JunitJSONTestList {
    public static void main(String[] args) {
        JSONObject testMethods = new JSONObject();

        if(args.length <= 0) System.exit(1);
        try {
            Class testClass = Class.forName(args[0]);

            //only list @Test-methods if class isn't abstract
            if(!Modifier.isAbstract(testClass.getModifiers())) {
                for(Method m : testClass.getMethods()) {
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        JSONObject testStructure = new JSONObject();
                        JSONObject extras = new JSONObject();
                        testMethods.put(testClass.getSimpleName()+"."+m.getName(), testStructure);

                        testStructure.put("extras", extras);

                        for(Annotation annotation : m.getAnnotations()) {
                            switch(annotation.annotationType().getName()) {
                            case "org.junit.Test":
                                //Ignore
                                break;
                            default:
                                JSONObject extra = new JSONObject();
                                extras.put(annotation.annotationType().getName(), extra);

                                for(Method method : annotation.annotationType().getMethods()) {
                                    if(method.getDeclaringClass() == Annotation.class) {
                                        continue;
                                    }

                                    String name = testClass.getSimpleName()+"."+method.getName();
                                    Object value = method.invoke(annotation);

                                    extra.put(name, value);
                                }
                            }
                        }
                    }
                }
            }

            System.out.println(testMethods);
        } catch (ClassNotFoundException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
            System.exit(2);
        }
    }
}
