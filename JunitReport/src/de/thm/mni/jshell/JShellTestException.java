package de.thm.mni.jshell;
import jdk.jshell.JShellException;
import jdk.jshell.EvalException;
import java.util.Map;
import java.util.Arrays;

/**
 * <p>Wraps a {@link JShellException} in order to provide better and more human readable
 * feedback.</p>
 * <p>This class should only be used in conjunction with {@link AbstractJShellTest},
 * because the error message directly refers to methods of this class.</p>
 */
public class JShellTestException extends Exception {
  private static final String msg = "An %s occurred during the\n"
    + "execution of your JShell code.\n\n"
    + "Hint: To find out, which test failed you can look at the next entry\n"
    + "after the method 'expressionResult'. If 'expressionResult' is not in\n"
    + "the stack trace, this means the exception was triggered directly from\n"
    + "your code. In both cases you will find the source of the actual\n"
    + "exception in your JShell code by scrolling down until you find a line\n"
    + "that says 'Caused by: ...'.";

    /**
     * <p>Creates a new exception based on the given cause.</p>
     * <p>Where possible, the snippet id in the stack trace of the
     * original exception will be replaced by <code>jShellFile</code>
     * and the line numbers will be mapped to the global input file
     * with the help of globalLineNumbers.</p>
     * @param cause the exception produced by the JShell
     * @param jShellFile the file name to use in the stack trace
     * @param globalLineNumbers starting line number of snippet by id
     */
  public JShellTestException(
    JShellException cause,
    String jShellFile,
    Map<String,Integer> globalLineNumbers
  ) {
    super(String.format(msg, getExceptionName(cause)), cause);
    translateJShellStackTrace(cause, jShellFile, globalLineNumbers);
  }

    /**
     * Small helper method to get the actual exception name if the
     * cause is an {@link EvalException}.
     * @param cause the base exception
     * @return the result of {@link EvalException#getExceptionClassName()} if
     * cause was an {@link EvalException}, otherwise the class name of cause.
     */
  private static String getExceptionName(JShellException cause) {
    if (cause instanceof EvalException) {
      return ((EvalException) cause).getExceptionClassName();
    }
    return cause.getClass().getName();
  }

    /**
     * Translates stack trace entries to show the given file name
     * and global line numbers instead of a snippet id and local
     * line numbers in those snippets.
     * @param ex base exception
     * @param jShellFile the file name to use
     * @param globalLineNumbers starting line number of snippet by id
     */
  private void translateJShellStackTrace(
          JShellException ex,
          String jShellFile,
          Map<String, Integer> globalLineNumbers
  ) {
    StackTraceElement[] trace = ex.getStackTrace();
    for(int i = 0; i < trace.length; i++) {
      if (!trace[i].getFileName().startsWith("#")) continue;
      // NOTE for some snippets, getLineNumber() returns an incorrect result
      // this is an error in the JShell implementation and we currently cannot
      // do anything about that :(
      // TODO add bug-id once it is posted online
      Integer lnr = globalLineNumbers.get(trace[i].getFileName().substring(1));
      if (lnr != null) {
        trace[i] = new StackTraceElement(
          trace[i].getClassLoaderName(),
          trace[i].getModuleName(),
          trace[i].getModuleVersion(),
          trace[i].getClassName(),
          trace[i].getMethodName(),
          jShellFile,
          trace[i].getLineNumber() + lnr - 1
        );
      }
    }
    ex.setStackTrace(trace);
  }
}