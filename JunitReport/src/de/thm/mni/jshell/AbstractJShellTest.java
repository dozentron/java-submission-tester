package de.thm.mni.jshell;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.Locale;
import java.util.Map;
import java.util.HashMap;

import org.junit.*;
import jdk.jshell.JShell;
import jdk.jshell.Diag;
import jdk.jshell.Snippet;
import jdk.jshell.SnippetEvent;
import jdk.jshell.VarSnippet;
import jdk.jshell.MethodSnippet;
import jdk.jshell.JShellException;
import jdk.jshell.SourceCodeAnalysis;
import static org.junit.Assert.*;

/**
 * <p>Base class for JShell tests in Dozentron. This class executes the JShell
 * code that is found in the file given by the system property
 * <code>jshellInput</code>. This is done once before each test case.
 * After the test case, all executed code is dropped, but the JShell instance
 * is reused for performance purposes.</p>
 *
 * <p>A test written with this base class only has to use the following three
 * methods to interact with the JShell. Everything else is taken care of by this
 * base class, including the translation of line numbers in stack traces and
 * user friendly error messages.</p>
 *
 * <ul>
 *     <li>{@link #varByName(String)}</li>
 *     <li>{@link #methodByName(String)}</li>
 *     <li>{@link #expressionResult(String)}</li>
 * </ul>
 */
public abstract class AbstractJShellTest {

  protected static String studentCode;
  protected static JShell shell;
  protected List<SnippetEvent> events;
  protected Map<String,Integer> globalLineNumbers;

    /**
     * Reads student code from the file given by the system property
     * <code>jshellInput</code>.
     * @throws IOException if the file could not be read
     */
  @BeforeClass
  public static void initializeStudentCode() throws IOException {
    studentCode = new String(
      Files.readAllBytes(Paths.get(System.getProperty("jshellInput"))),
      StandardCharsets.UTF_8
    );
  }

    /**
     * <p>Creates a new JShell instance to use for tests.</p>
     * <p>This instance will be shared across all test cases.</p>
     */
  @BeforeClass
  public static void createJShell() {
    shell = JShell.create();
  }

    /**
     * <p>Returns a representation of the variable with the given name.</p>
     * <p>If no such variable exists in the JShell, {@link Assert#fail(String)}
     * will be called.</p>
     * @param name the name of the variable
     * @return snippet that can be used to inspect the variable (type, value, ...)
     */
  public VarSnippet varByName(String name) {
    Optional<VarSnippet> opt = shell.variables().filter(x -> x.name().equals(name)).findFirst();
    if (!opt.isPresent()) fail("There is no variable named "+name);
    return opt.get();
  }

    /**
     * <p>Returns a representation of the method with the given name.</p>
     * <p>If no such method exists in the JShell, {@link Assert#fail(String)}
     * will be called.</p>
     * @param name the name of the method
     * @return snippet that can be used to inspect the method (return type, parameters, ...)
     */
  public MethodSnippet methodByName(String name) {
    Optional<MethodSnippet> opt = shell.methods().filter(x -> x.name().equals(name)).findFirst();
    if (!opt.isPresent()) fail("There is no method named "+name);
    return opt.get();
  }

    /**
     * Rethrows the first exception (if it exists) in the given list of SnippetEvents.
     * @param events events that could have triggered an exception
     * @throws JShellTestException if there was an exception in the events
     */
  private void rethrowJShellExceptions(List<SnippetEvent> events) throws JShellTestException {
    Optional<SnippetEvent> exceptionEvent = events.stream()
      .filter(x -> x.exception() != null)
      .limit(1)
      .findFirst();
    if (exceptionEvent.isPresent()) {
      JShellException ex = exceptionEvent.get().exception();
      throw wrapTestException(ex);
    }
  }

    /**
     * <p>Evaluates the given expression on the JShell and returns the
     * result as a string.</p>
     * @param exp the expression to evaluate
     * @return the string representation of the expression value
     * @throws JShellTestException if the expression raised an exception
     */
  public String expressionResult(String exp) throws JShellTestException {
    List<SnippetEvent> expEvents = shell.eval(exp);
    rethrowJShellExceptions(expEvents);
    failOnRejectedSnippets(expEvents);
    return expEvents.get(expEvents.size()-1).value();
  }

    /**
     * Wraps the given exception in a {@link JShellTestException}.
     * @param cause the exception to wrap
     * @return the wrapped exception with additional feedback
     */
  private JShellTestException wrapTestException(JShellException cause) {
    return new JShellTestException(
      cause,
      System.getProperty("jshellInput"),
      globalLineNumbers
    );
  }

    /**
     * <p>Executes the student code in the JShell instance.</p>
     * <p>This method does not check for errors, since JShell code can
     * also be partly valid.</p>
     */
  @Before
  public void setupJShell() {
    globalLineNumbers = new HashMap<>();
    events = new ArrayList<SnippetEvent>();
    SourceCodeAnalysis anal = shell.sourceCodeAnalysis();
    SourceCodeAnalysis.CompletionInfo info;
    String src = studentCode;
    int lineNr = 1;
    do {
      info = anal.analyzeCompletion(src);
      List<SnippetEvent> snippetEvents = shell.eval(info.source());
      events.addAll(snippetEvents);
      for(SnippetEvent se : snippetEvents) {
        globalLineNumbers.putIfAbsent(se.snippet().id(), lineNr);
      }
      lineNr += getLineNumber(info.source(), info.source().length()) - 1;
      src = info.remaining();
    } while (src.length() > 0);
  }

    /**
     * Drops all snippets in the JShell so that it can be used for
     * the next test.
     */
  @After
  public void resetJShell() {
    shell.snippets().forEach(shell::drop);
  }

    /**
     * <p>Small test case that checks if there where any exceptions thrown in
     * the code that was written by the student.</p>
     * @throws JShellTestException if the student code raises an exception
     */
  @Test
  public void testJShellExceptions() throws JShellTestException {
    rethrowJShellExceptions(events);
  }

    /**
     * <p>Small test case that checks if there where any rejected snippets
     * (aka compiler errors) in the code that was written by the student.</p>
     * @throws AssertionError with detailed compiler information if there
     *                        where any problems
     */
  @Test
  public void testJShellRejected() {
    failOnRejectedSnippets(events);
  }

    /**
     * <p>Determines the line number of the character at the given position
     * in the string (starting at 1).</p>
     * <p>Works for Windows-, Unix- and Mac-style line endings (which is
     * surprisingly difficult ;P).</p>
     * @param haystack the (multiline) string
     * @param needle the character index within the string
     * @return the line number of needle inside haystack (starting at 1)
     */
  private static int getLineNumber(String haystack, int needle) {
    int lineNr = 1;
    char last = '\0';
    for(int i = 0; i < needle; i++) {
      char c = haystack.charAt(i);
      if (last == '\r' && c == '\n') continue;
      if (c == '\n' || c == '\r') lineNr++;
      last = c;
    }
    return lineNr;
  }

    /**
     * <p>Converts a global character index to an index from the beginning
     * of the current line.</p>
     * <p>Works for Windows-, Unix- and Mac-style line endings.</p>
     * @param haystack the (multiline) string
     * @param needle the global character index
     * @return local character index from the beginning of the line
     */
  private static int indexWithinLine(String haystack, int needle) {
    int lastN = haystack.lastIndexOf("\n", needle);
    int lastR = haystack.lastIndexOf("\r", needle);
    int lastNewline = Math.max(lastR, lastN);
    if (lastNewline == -1) return needle;
    return needle - lastNewline;
  }

    /**
     * Finds the line number in the student input, where the given
     * snippet starts.
     * @param s snippet to locate
     * @return location (line number starting at 1) of snippet in student input
     */
  private int getGlobalLineNumber(Snippet s) {
    return globalLineNumbers.getOrDefault(s.id(),-1);
  }

    /**
     * Converts a diagnostic object to a meaningful error message
     * with correct global line numbers.
     * @param s snippet that raised an error
     * @param d diagnostic for the error
     * @return human readable error message
     */
  private String diagnosticToString(Snippet s, Diag d) {
    StringBuilder sb = new StringBuilder();
    sb.append("Compiler error in following snippet:\n");
    // TODO maybe reduce the number of lines shown for large methods/classes?
    sb.append("##### Snippet start #####\n");
    int ln = 1;
    for (String line : s.source().split("(\n)|(\r\n?)")) {
      sb.append(String.format("%2d|%s\n",ln,line));
      ln++;
    }
    sb.append("###### Snippet end ######\n");
    int lg = getGlobalLineNumber(s);
    int ls = getLineNumber(s.source(), (int) d.getStartPosition());
    int le = getLineNumber(s.source(), (int) d.getEndPosition());
    int cs = indexWithinLine(s.source(), (int) d.getStartPosition());
    int ce = indexWithinLine(s.source(), (int) d.getEndPosition());
    sb.append(String.format(
      "Error start: Line %3d (%3d in source), character %3d\n",
      ls,
      lg < 0 ? -1 : lg + ls - 1,
      cs
    ));
    sb.append(String.format(
      "Error end  : Line %3d (%3d in source), character %3d\n",
      le,
      lg < 0 ? -1 : lg + le - 1,
      ce
    ));
    sb.append("Message:\n");
    sb.append(d.getMessage(Locale.getDefault()));
    return sb.toString();
  }

    /**
     * Calls {@link Assert#fail(String)} on any rejected snippets, using
     * {@link #diagnosticToString(Snippet, Diag)} for readable error messages.
     * @param events the list of snippet events that may contain rejections
     */
  private void failOnRejectedSnippets(List<SnippetEvent> events) {
    Optional<String> failMsg = events.stream()
      .filter(e -> e.status() == Snippet.Status.REJECTED)
      .map(e -> 
        shell.diagnostics(e.snippet())
          .map(d -> diagnosticToString(e.snippet(), d))
          .collect(Collectors.joining("\n------\n"))
      )
      .findFirst();
    // note: we fail outside of stream to have a tidy stack trace
    if (failMsg.isPresent()) {
      fail(failMsg.get());
    }
  }

    /**
     * Closes the JShell instance.
     */
  @AfterClass
  public static void destroyJShell() {
    shell.stop();
  }
}
