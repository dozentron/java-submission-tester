require 'logger'

module JavaSubmissionTester
  class Engine < ::Rails::Engine
    isolate_namespace JavaSubmissionTester

    JAVA_VERSION_REGEX = /(\d+)\.(\d+)\.\d+/

    def log_info(*args)
      Rails.logger.info "==> JST: #{args.join(" ")}"
    end
    def log_error(*args)
      Rails.logger.error "==> JST: #{args.join(" ")}"
    end
    def log_warn(*args)
      Rails.logger.warn "==> JST: #{args.join(" ")}"
    end

    def check_java_config(java_config, version)
      errors = false
      executables = java_config.select{ |k,v| k != :jvm_cfg && k != :java_home }
      executables.select{|k, v| not File.executable?(v) }.each{ |k,v|
        log_error "#{k} (java version:#{version}): Not executable: #{v}"
        errors = true
      }

      if not errors # only test if it's executable
        executables.select {|k,v|
          stdout = `#{v} -version 2>&1`
          match_obj = JAVA_VERSION_REGEX.match(stdout)
          if not match_obj
            true
          else
            match_obj[1].to_i != version &&
            match_obj[2].to_i != version
          end
        }.each{ |k,v|
          log_error "#{k} (java version:#{version}): Doesn't seem to be the expected java executable: #{v}"
          errors = true
        }
      end

      errors
    end


    initializer "java_submission_tester.assets.precompile" do |app|
      app.config.assets.precompile += %w( java_submission_tester/*.png java_submission_tester/*.svg java_submission_tester/*.css java_submission_tester/*.js)
      end

    initializer "java_submission_tester.check-paths" do |app|
      if JavaSubmissionTester.skip_test
        log_warn "Skipping any config tests!!"
      else

      log_info "checking paths"
      log_info "checking bubblewrap dependencies"
      error = false
      JavaSubmissionTester.bubblewrap_dependencies
                       .select { |dep| not (Dir.exist?(dep) || File.exist?(dep)) }
                        .each { |dep|
                          log_error "#{dep} doesn't exist!"
                          error = true
      }

      log_info "bubblewrap dependencies ok!" unless error

      if error
        raise "Couldn't initialize java_submission_tester. Check log for details!"
      end
    end
    end
  end
end
