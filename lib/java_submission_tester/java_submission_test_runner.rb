# coding: utf-8
require 'open3'
require 'java_submission_tester/java_helper'

module JavaSubmissionTester
  class JavaSubmissionTestRunner
    include ::JavaSubmissionTester::JavaHelper

    LANGUAGE_NAME = 'Java'

    def validate_test_structure(test_file, stub_file)
      validation_message = validate_is_jar? test_file
      validation_message = validate_is_jar? stub_file

      return {error: validation_message} unless validation_message.nil?

      run_unzipped test_file do |path|
        pathname = Pathname.new path

        only_source_code_test = false

        Dir[pathname + '**/SourceCodeTest.*'].each do |f|
          File.delete(f)

          if Dir.empty?(File.dirname(f))
            only_source_code_test = true
          end
        end

        validation_message = 'Test muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Test muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        compiled, error = compiles?(path)
        validation_message = 'Test compiliert ohne Abgabe. Test enthält vermutlich die zu testende Klasse und verdeckt sie somit. Bitte entfernen sie die zu testende Klasse aus der Jar-Datei.' if compiled && !only_source_code_test

        break unless validation_message.nil?

        unzip stub_file.current_path, path

        compiled, error = compiles?(path)
        validation_message = ['Test kann mit dem angegeben Stub nicht compiliert werden.', error] unless compiled
      end

      return {error: validation_message}
    end

    def validate_submission_structure(submission_file, task)
      testJars = task.test_jar_list

      validation_message = validate_is_jar? submission_file

      return {error: validation_message} unless validation_message.nil?

      run_unzipped submission_file do |path|
        pathname = Pathname.new path

        validation_message = 'Abgabe muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Abgabe muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        missing_source_class_pairs = []

        Dir[pathname + '**/*.class'].each do |class_file|
          next if class_file.include?('$')

          corresponding_source_file = File.dirname(class_file) + '/' + File.basename(class_file, ".*") + ".java"
          unless File.exist? corresponding_source_file
            missing_source_class_pairs.append(source_file: corresponding_source_file, class_file: class_file)
          end
        end

        if missing_source_class_pairs.any?
          validation_message = ["Zu mindestens einer .class-Datei wurde keine .java-Datei gefunden!\n",
                                "Zu mindestens einer .class-Datei wurde keine .java-Datei gefunden!\n"+
                                missing_source_class_pairs.map{|p| "Zur Datei #{p[:class_file].remove(pathname.to_s)} fehlt #{p[:source_file].remove(pathname.to_s)}"}.join("\n")]
        end

        break unless validation_message.nil?

        # This is a somewhat ineffective method to check if the sources are in the given package.
        Dir[pathname + '**/*.java'].each do |source_file|
          package = nil
          real_package = ''
          File.open(source_file).each do |line|
            next unless line =~ /\A(\s*package.*)\Z/
            package = /\s*package\s+([a-zA-Z.]+)/.match(line)[1].gsub('.', '/') + '/'
            real_package = source_file.gsub(pathname.to_s + '/', '').sub(File.basename(source_file), '')
            break
          end
          unless (real_package.empty? && package.nil?) || (real_package.eql? package)
            validation_message = ["Eine Quelldatei befindet sich nicht an dem Ort, der laut package-Anweisung vorgegeben ist!",
                               "Eine Quelldatei befindet sich nicht an dem Ort, der laut package-Anweisung vorgegeben ist!\n"+
                               "  Erwartet: #{package.to_s + File.basename(source_file)}\n"+
                               "  Tatsächlich: #{source_file.gsub(pathname.to_s, '')}"]
          end
          break unless validation_message.nil?
        end

        break unless validation_message.nil?

        validation_message = validate_class_version Dir[pathname + '**/*.class'].first

        break unless validation_message.nil?

        testJars.each do |t|
          unzip t.current_path, path # Unzip now because it will break the above checks
        end

        compiled, error = compiles?(path)
        validation_message = ['Abgabe kann nicht compiliert werden.', error] unless compiled
      end

      warnings = check_for_garbage(submission_file, task)
      return {error: validation_message, warning: warnings}
    end

    def moss_language
      MOSS_LANGUAGE
    end

    # calls block with all source file contents that may be relevant to moss
    def source_files(carrierwave_uploader)
      run_unzipped carrierwave_uploader do |path|
        path_name = Pathname.new path

        source_files = Dir[path_name + '**/*.java']

        source_files.each do |source_file|
          yield File.basename(source_file), File.open(source_file)
        end
      end
    end

    def test(task, submission, test_suit)
      result = nil
      testJars = task.test_jar_list

      run_unzipped submission.submission, *testJars do |working_dir|
        #before execution: generate a fresh jar from students code

        student_jar, error = create_fresh_jar(submission.submission)
        raise "error occured while generating jar: #{error}" if error

        fresh_jar, error = create_fresh_jar_from_directory(working_dir)
        raise "error occured while generating jar: #{error}" if error

        result = run_json_reporter(working_dir,
                                   test_suit,
                                   {jars: [fresh_jar, student_jar],
                                    properties: ["-Dstudent-jar=./#{File.basename(student_jar)}",
                                                 encoding_property()
                                                ]
                                   })

        File.delete(fresh_jar) #remove our garbage ;)
        File.delete(student_jar)

        # removes unnecessary exception text if it is a result from the static code analyser
        if result != nil && result != 'Timeout exceeded'
            result.each do |key, value|
            if (result[key]['successful'] == false)
                trimStart = result[key]['failureTrace'].index('TRIM START')
                if trimStart != nil
                    trimEnd = result[key]['failureTrace'].index('TRIM END')
                    if trimEnd != nil
                        result[key]['failureTrace'] = result[key]['failureTrace'][trimStart+10..trimEnd-1]
                    end
                end
            end
        end

      end
      return result
    end
    end

    private
    def check_for_garbage(submission_file, task)
      submission_filenames = zip_entries(submission_file)
      test_filenames = task.test_jar_list.map{ |t| zip_entries(t) }.sum

      garbage_files = submission_filenames.select do |filename|
        file_downcased = filename.downcase
        !test_filenames.include?(filename) &&
          !file_downcased.end_with?(".class") &&
          !file_downcased.end_with?(".java") &&
          !file_downcased.end_with?(".mf")
      end

      unless garbage_files.empty?
        return "Die folgenden Dateien sollten nicht in der jar sein:\n  #{garbage_files.join("\n  ")}"
      end
    end

    #creates a new jar from the students code, inside of the given working directory.
    #returnsa as tuple the jarpath and eventual compile errors (jarpath,errors).
    def create_fresh_jar(origin_jar)
      run_unzipped origin_jar do |path|
        Dir[path+"/**.jar"].each{ |f| File.delete(f) }
        compiled, error = compiles?(path)
        if compiled
          #generate a new jar with fresh '.class' files from the current working directory
          orig_name = File.basename(origin_jar.current_path, '.jar')
          jarpath = "/tmp/submission-#{orig_name}-#{SecureRandom.uuid}.jar"
          Rails.logger.debug "Generating #{jarpath}"
          stdout = `cd #{path} ; zip -r #{jarpath} ./**`
          Rails.logger.debug "zipping returned: #{stdout}"
          return jarpath, nil
        else #compiler error
          return nil, error
        end
      end
    end

    def create_fresh_jar_from_directory(path)
      Dir[path+"/**.jar"].each{ |f| File.delete(f) }
      compiled, error = compiles?(path)
      if compiled
        #generate a new jar with fresh '.class' files from the current working directory
        orig_name = File.basename(path, '.jar')
        jarpath = "/tmp/submission-#{orig_name}-#{SecureRandom.uuid}.jar"
        Rails.logger.debug "Generating #{jarpath}"
        stdout = `cd #{path} ; zip -r #{jarpath} ./**`
        Rails.logger.debug "zipping returned: #{stdout}"
        return jarpath, nil
      else #compiler error
        return nil, error
      end
    end
  end
end
