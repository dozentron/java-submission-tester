# coding: utf-8
require 'zip'
require 'open3'
require 'mimemagic'

module JavaSubmissionTester
  class JShellSubmissionTestRunner
    include ::JavaSubmissionTester::JavaHelper

    LANGUAGE_NAME = 'JShell'

    def validate_test_structure(test_file, stub_file)
      validation_message = validate_is_jar? test_file
      return validation_message unless validation_message.nil?

      run_unzipped test_file do |path|
        pathname = Pathname.new path

        validation_message = 'Test muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Test muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        compiled, error = compiles?(path)

        puts "\n\n #{error}\n\n"

        validation_message = ['Test compiliert nicht. JShell Tests müssen ohne Stub compilieren.',error] unless compiled
      end
      return validation_message
    end

    def validate_submission_structure(submission_file, test_file)
      #nothing to validate
    end

    def moss_language
      MOSS_LANGUAGE
    end

    def test(task, submission, test_suit)
      result = nil
      testJars = task.test_jar_list
      run_unzipped *testJars do |working_dir|
        result = run_json_reporter(working_dir,
                                   test_suit,
                                   {files: [submission.submission.current_path],
                                    jars: testJars.map{ |e| e.current_path.to_s },
                                    #through our bwrap: the input file will be placed in the cwd of java
                                    properties:  "-DjshellInput=#{File.basename(submission.submission.current_path)}"})
      end
      return result
    end
  end
end
