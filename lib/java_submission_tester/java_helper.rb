# coding: utf-8
require 'zip'
require 'open3'
require 'mimemagic'

module JavaSubmissionTester
  module JavaHelper
    LIB_DIR =  Pathname.new(File.dirname(__FILE__)).join("../jars")
    JAR_DIRECTORY = LIB_DIR.to_s
    JUNIT_PATH = LIB_DIR.join('junit-4.12.jar').to_s
    HAMCREST_CORE_PATH = LIB_DIR.join('hamcrest-core-1.3.jar').to_s
    HAMCREST_LIB_PATH = LIB_DIR.join('hamcrest-library-1.3.jar').to_s
    CODE_ANALYSER_LIB_PATH = LIB_DIR.join('codeanalyser.jar').to_s
    JUNIT_REPORT_PATH = LIB_DIR.join('JunitReport.jar').to_s
    JUNIT_CLASSPATH = Dir[LIB_DIR.join("*.jar").to_s].join(":")
    MOSS_LANGUAGE = 'java'
    TEST_TIMEOUT_DURATION = '2m'


    def student_help_path
      # ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:student_help_submission)
      '/doc#_java'
    end
    def teacher_help_path
      # ::JavaSubmissionTester::Engine.routes.url_helpers.pages_path(:teacher_help_submission)
      "/doc#_wie_erstelle_ich_eine_abgabe_in_dozentron"
    end

    def jarlist_to_classpath(jarlist)
      return jarlist.map { |e|  e.current_path }.join(":")
    end

    def validate_is_jar?(carrierwave_uploader)
      mime_type_path = MimeMagic.by_path(carrierwave_uploader.current_path)
      mime_type_magic = MimeMagic.by_magic(carrierwave_uploader.file.read)
      jar_error = 'Die Datei muss ein java-Archiv (jar) sein.'

      return jar_error unless mime_type_path.nil? || mime_type_path == 'application/x-java-archive' || mime_type_path == 'application/java-archive'
      return jar_error unless mime_type_magic.nil? || mime_type_magic == 'application/x-java-archive' || mime_type_magic == 'application/java-archive' || mime_type_magic == 'application/zip'

      #Teste das eigentlich wichtige, kann ich es als Zip öffnen
      begin
        Zip::File.open(carrierwave_uploader.current_path) {}
      rescue
        return jar_error
      end
    end

    def test_suit_names_for(testJars, stub)
      test_suites = nil

      run_unzipped *testJars do |path|
        path_name = Pathname.new path

        test_suite_selection = Dir[path_name + '**/Test*.class'] + Dir[path_name + '**/*Test.class']
        test_suite_selection.select! {|p| File.file? p}
        test_suite_selection.map! {|p| Pathname.new(p).relative_path_from(path_name).to_s}
        test_suite_selection.map! {|p| p.sub(/.class$/, '').gsub(::File::SEPARATOR, '.')}

        test_suites = test_suite_selection
      end

      return test_suites
    end

    def test_cases_for_testsuit(testJars, stub, test_suit_name)

      begin
        class_path = "#{::JavaSubmissionTester::JavaHelper::JUNIT_REPORT_PATH}:#{::JavaSubmissionTester::JavaHelper::JUNIT_CLASSPATH}:#{jarlist_to_classpath(testJars)}"
        class_path = "#{class_path}:#{stub.current_path}" unless stub.current_path.nil?

        cmd = "#{java_path()[:java]} -cp \"#{class_path}\" de.thm.mni.JunitJSONTestList \"#{test_suit_name}\""
        stdout_string, stderr_string, status = Open3.capture3(cmd)

        if status.exitstatus == 0
          result = JSON.parse stdout_string
          return result
        else
          raise "Test konnte mit dem übergebenen Stub nicht geladen werden. Java meldet:\n#{stderr_string}"
        end
      rescue JSON::ParserError => e
        Rails.logger.error "#{e.to_s}\n#{"Beim bestimmen der Testfälle wurde ungültiges JSON erstellt, dies sollte nicht möglich sein!\nstdout: #{stdout_string}\nstderr: #{stderr_string}"}"
        raise "Beim bestimmen der Testfälle wurde ungültiges JSON erstellt, dies sollte nicht möglich sein!\nstdout: #{stdout_string}\nstderr: #{stderr_string}"
      end
    end

    def run_unzipped(*carrierwave_uploaders)
      if block_given?
        Dir.mktmpdir do |tmp_dir|
          carrierwave_uploaders.each do |carrierwave_uploader|
            unzip(carrierwave_uploader.current_path, tmp_dir)
          end

          yield tmp_dir
        end
      else
        raise 'run_unzipped needs a block to run!'
      end
    end

    def generate_input(input_generator_uploader)
      status_ok, std_err, std_out = runnable?(input_generator_uploader.current_path)

      raise "There was an problem with the input Generator\nExcecution ended with error:\n#{std_err}" unless status_ok
      return std_out
    end

    def runnable?(jar_file)
      cmd = "#{java_path()[:java]} -jar \"#{jar_file}\""
      stdout_string, stderr_string, status = Open3.capture3(cmd)

      return status.exitstatus == 0, stderr_string, stdout_string
    end

    def validate_class_version(class_file_path)
      cmd = "#{java_path()[:javap]} -verbose #{class_file_path} | grep major"

      stdout_string, stderr_string, status = Open3.capture3(cmd)

      version = /\d+/.match stdout_string.strip
      # https://en.wikipedia.org/wiki/Java_class_file, in general layouts are the java Versions listed
      # Java SE 12 = 56 (0x38 hex)
      # Java SE 11 = 55 (0x37 hex)
      # Java SE 10 = 54 (0x36 hex)
      # Java SE 9  = 53 (0x35 hex)
      # Java SE 8  = 52 (0x34 hex),
      return 'Abgabe wurde compiliert mit Java > 12. Unterstützt werden Java <= 12' if version[0].to_i > 56
    end

    def compiles?(working_dir)
      class_path = "#{JUNIT_REPORT_PATH}:#{JUNIT_CLASSPATH}:#{working_dir}"

      cmd = "#{java_path()[:javac]} -encoding UTF-8 --enable-preview -source 12 -cp \"#{class_path}\" \"#{Dir["#{working_dir}/**/*.java"].join('" "')}\""

      Rails.logger.info("Compiling: #{cmd}")

      stdout_string, stderr_string, status = Open3.capture3(cmd)
      Rails.logger.info("Compiling returned: out: #{stdout_string} err: #{stderr_string}")
      return status.exitstatus == 0, stderr_string
    end

    def unzip(file, dest_dir)
      dest_dir = Pathname.new dest_dir
      Zip::File.open(file) do |zip_file|
        zip_file.each do |entry|
          output_path = dest_dir.join entry.name.strip
          FileUtils.mkdir_p output_path.dirname
          zip_file.extract(entry, output_path) {true}
        end
      end
    end

    def zip_entries(uploader)
      Zip::File.open(uploader.current_path) do |zip_file|
        zip_file
          .map { |entry| entry.name }
          .select { |filename| !filename.end_with?("/") }
          .to_a
      end
    end

    def java_path
      return JavaSubmissionTester.java_path
    end

    def create_policy(submission_codebase_path, test_paths, working_dir)
      #each test jar is readable & has every permission
      readTestPermissions = test_paths.map { |e| "permission java.io.FilePermission \"#{e.current_path}\", \"read\";"  }.join("\n")
      testCodeBasePermissions = test_paths.map { |e| "grant codeBase \"file:#{e.current_path}\" { permission java.security.AllPermission; };"  }.join("\n")

      policy_file_content = %{grant {
        permission java.lang.reflect.ReflectPermission "suppressAccessChecks";
        permission java.lang.RuntimePermission "*";
        permission java.util.PropertyPermission "*", "read";
        permission java.io.FilePermission "#{java_path()[:java_home]}/lib/modules", "read";
        permission java.io.FilePermission "#{JAR_DIRECTORY}/*", "read";
        #{readTestPermissions}
        permission java.io.FilePermission "#{submission_codebase_path}", "read";
        permission java.io.FilePermission "#{working_dir}/*", "read,write";
      };
      grant codeBase "file:#{JAR_DIRECTORY}/*" {
        permission java.security.AllPermission;
      };
      #{testCodeBasePermissions}
      }

      policy_file = Tempfile.new 'deny_all.policy', encoding: 'UTF-8'
      policy_file.write policy_file_content
      policy_file.close
      policy_file.path
    end


    def run_json_reporter(working_dir,test_suit,options)
      output_file = Tempfile.new encoding: 'UTF-8'
      output_file.close

      jars =if options[:jars].nil? then []
            elsif options[:jars].is_a?(Array) then options[:jars]
            else [options[:jars]]
            end
      files =if options[:files].nil? then []
             elsif options[:files].is_a?(Array) then options[:files]
             else [options[:files]]
             end

      java_cmd = java_cmd(output_file.path,test_suit,options)
      bwrap = setup_bubblewrap(jars,files << output_file.path,java_cmd)

      Rails.logger.info("Running: #{bwrap}")

      #stdout_string, stderr_string, status = Open3.capture3(bwrap) # => outputs to output_file
      system(bwrap, out: File::NULL)

      #Rails.logger.info("Running returned: code: #{status}, out: #{stdout_string} err: #{stderr_string}")
      Rails.logger.info("Running returned: code: #{$?.exitstatus}")

      if $?.exitstatus == 124
        result = 'Timeout exceeded'
      else
        result = JSON.load File.open(output_file,"r:UTF-8")
        output_file.close! #close and Delete tempfile
      end
      return result
    end

    #Sets up a bubblewrap environment.
    #The Environment looks like this:
    # /tmp
    # ├── Input.jshell
    # ├── JShellTest.jar
    # ├── junit-output.txt
    # └── libs
    #     ├── hamcrest-core-1.3.jar
    #     ├── hamcrest-library-1.3.jar
    #     ├── junit-4.12.jar
    #     └── JunitReport.jar
    # - r_files a list of files that are linked read-only. They are placed in the working directory of the bwrap environment (/tmp)
    # - rw_files a list of files that are linked read/write. They are placed in the working directory of the bwrap environment (/tmp)
    # - cmd the command that should run inside the sandbox
    def setup_bubblewrap(r_files, rw_files,cmd)
      #setup a bubblewrap environment:
      # - include needed header & binaries from /usr, /usr/lib
      # - include needed /proc files
      # - use a sandboxed /tmp directory as current directory
      # - include all library jars in /tmp/libs
      # - place tests, student submission and all submission related files in /tmp

      bwrap_cwd = "/tmp"
      r_str = r_files.map { |s| "--ro-bind #{s} #{bwrap_cwd}/#{File.basename(s)}" }.join(' ')
      rw_str = rw_files.map { |s| "--bind #{s} #{bwrap_cwd}/#{File.basename(s)}" }.join(' ')
      runtime_deps = JavaSubmissionTester.bubblewrap_dependencies.map { |e| "--ro-bind #{e} #{e}" }.join(' ')

      return %Q{#{JavaSubmissionTester.timeout} #{TEST_TIMEOUT_DURATION}
    #{JavaSubmissionTester.bwrap}
      #{runtime_deps}
      --dir #{bwrap_cwd}
      --dir /var
      --symlink ../tmp var/tmp
      --proc /proc
      --chdir #{bwrap_cwd}
      --unshare-all
      --ro-bind #{LIB_DIR} #{bwrap_cwd}/libs
      #{r_str}
      #{rw_str}
      #{cmd}
      }.gsub(/\s+/, ' ')
    end

    def java_cmd(output_file,test_suit,options)
      additional_props = if options[:properties].nil? then ''
                         elsif options[:properties].is_a?(Array) then options[:properties].join(" ")
                         else options[:properties]
                         end
      additional_cp = if options[:jars].nil? then ''
                      elsif options[:jars].is_a?(Array) then options[:jars].map{ |p| File.basename(p) }.join(":")
                      else File.basename(options[:jars])
                      end

      #our bwrap places all lib-jars inside of ./libs/[library].jar
      library_cp = Dir[LIB_DIR+"*.jar"].map { |p| "libs/#{File.basename(p)}"}.join(":")

      return %Q{
  #{java_path()[:java]} #{additional_props}
  --enable-preview
  -cp #{library_cp}:#{additional_cp}
  de.thm.mni.JunitJSONReportRunner #{output_file} #{test_suit}
  }.gsub(/\s+/, ' ')
    end

    def encoding_property
      if JavaSubmissionTester.utf8_trolling
        "-Dfile.encoding=UTF-32"
      else
        "-Dfile.encoding=UTF-8"
      end
    end
  end
end
