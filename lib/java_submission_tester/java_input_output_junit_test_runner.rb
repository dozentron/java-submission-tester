# coding: utf-8
require 'open3'
require 'java_submission_tester/java_helper'

module JavaSubmissionTester
  class JavaInputOutputJunitTestRunner
    include ::JavaSubmissionTester::JavaHelper
    LANGUAGE_NAME = 'Java8 Junit4'

    JUNIT_PATH = Pathname.new(File.dirname(__FILE__)).join('junit-4.12.jar').to_s
    HAMCREST_CORE_PATH = Pathname.new(File.dirname(__FILE__)).join('hamcrest-core-1.3.jar').to_s
    JUNIT_CLASSPATH = "#{JUNIT_PATH}:#{HAMCREST_CORE_PATH}"
    JUNIT_REPORT_PATH = Pathname.new(File.dirname(__FILE__)).join('JunitReport.jar').to_s

    def validate_submission_structure(submission_file, test_file)

    end

    def validate_test_structure(test_file, stub_file)
      validation_message = validate_is_jar? test_file
      return validation_message unless validation_message.nil?

      run_unzipped test_file do |path|
        pathname = Pathname.new path

        validation_message = 'Test muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Test muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        validation_message = validate_class_version Dir[pathname + '**/*.class'].first

        break unless validation_message.nil?

        compiled, error = compiles?(path)
        validation_message = ['Test kann nicht compiliert werden!', error] unless compiled
      end

      return validation_message
    end

    def validate_input_generator_structure(test_file, stub_file)
      validation_message = validate_is_jar? stub_file
      return validation_message unless validation_message.nil?

      run_unzipped stub_file do |path|
        pathname = Pathname.new path

        validation_message = 'Test muss die Java Source Dateien enthalten!' if Dir[pathname + '**/*.java'].empty?
        validation_message = 'Test muss die Class Dateien enthalten!' if Dir[pathname + '**/*.class'].empty?

        break unless validation_message.nil?

        validation_message = validate_class_version Dir[pathname + '**/*.class'].first

        break unless validation_message.nil?

        runnable, error, out = runnable?(stub_file.current_path)

        validation_message = 'Input Generator produziert keinen Input auf stdout!', error if out.strip.empty?
        validation_message = 'Input Generator ist nicht ausführbar!', error unless runnable
      end

      return validation_message
    end

    def test(task, submission, test_suit)
      user = submission.user
      input = task.input(user)
      testJars = task.test_jar_list
      result = nil

      run_unzipped *testJars do |working_dir|
        #text files uploaded using carrierwave always have a basename of 'Text' (rails/private/.../Text)
        #in order to determine input & output we are copying both files into 2 temp files: input-[ID]XXXXX and output-[ID]XXXXX
        Tempfile.open("input-#{task.id}") do |input_file|
          input_file.write input.input.read()
          Tempfile.open("output-#{submission.id}") do |output_file|
            output_file.write submission.output.read().force_encoding("UTF-8")
            puts "input file:",input_file.read()
            puts "output file:",output_file.read()
            result = run_json_reporter(working_dir,
                                       test_suit,
                                       {files: [input_file.path,output_file.path],
                                        jars: testJars.map{ |e| e.current_path.to_s },
                                        properties:  ["-Dinput=#{File.basename(input_file)}",
                                                      "-Dstudent-output=#{File.basename(output_file)}"]})
          end
        end
      end
      return result
    end
  end
end
