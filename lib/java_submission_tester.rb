require 'java_submission_tester/engine'

module JavaSubmissionTester
  # Your code goes here...
  mattr_accessor :bwrap
  mattr_accessor :timeout
  mattr_accessor :java_path
  mattr_accessor :bubblewrap_dependencies
  mattr_accessor :skip_test
  mattr_accessor :utf8_trolling

  self.bubblewrap_dependencies = [
    "/usr",
    "/lib",
    "/lib64",
    "/sbin"
  ]

  self.bwrap = "/usr/bin/bwrap"
  self.timeout = "/usr/bin/timeout"

  self.skip_test = false
  self.utf8_trolling = false
  self.java_path = {
      java: '/usr/lib/jvm/java-8-openjdk/bin/java',
      javac: '/usr/lib/jvm/java-8-openjdk/bin/javac',
      javap: '/usr/lib/jvm/java-8-openjdk/bin/javap'
  }
end
