$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "java_submission_tester/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "java_submission_tester"
  s.version     = JavaSubmissionTester::VERSION
  s.authors     = ["Dominic Althaus"]
  s.email       = ["althaus.dominic@gmail.com"]
  s.homepage    = "https://git.thm.de/dalt40/java-submission-tester"
  s.summary     = "allows testing of java applications"
  s.description = "allows testing of java applications"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 5.0.0.rc1", "< 5.1"
  s.add_dependency "rubyzip"
  s.add_dependency "mimemagic"
  s.add_dependency 'slim'

  s.add_development_dependency "sqlite3"
end
